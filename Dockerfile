FROM nginx:latest

WORKDIR /family_app

RUN mkdir backend frontend  nginx

COPY ./backend/. /family_app/backend

RUN rm ./backend/ormconfig.json && mv ./backend/ormconfig.dev.json ./backend/ormconfig.json 

COPY ./frontend/. /family_app/frontend
# copy nginx config 
COPY ./nginx/. /family_app/nginx

# download node.js bash script and run it 
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -

# install node.js
RUN apt-get install -y nodejs

# install pm2
RUN npm install pm2 -g

# build backend code for production
RUN cd ./backend && npm install && npm install -D && npm run build && cd ..

# build frontend code for production
RUN cd ./frontend && npm install && npm install -D && npm run build && cd ..

# remove default nginx config
RUN rm /etc/nginx/conf.d/default.conf

# mv nginx config to proper nginx configuration
RUN mv ./nginx/default.conf /etc/nginx/conf.d

RUN mkdir -p  /usr/share/nginx/html/family_app && mv ./frontend/dist/*  /usr/share/nginx/html/family_app

# start backend app with pm2
CMD ["pm2-runtime", "./backend/dist/app.js"]

