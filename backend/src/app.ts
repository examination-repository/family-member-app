import db from './config/db';
import express, { Application } from 'express';
import cors from 'cors';
import { config } from 'dotenv';

config();

//* import all routes in here
import familyRoutes from './routes/family.routes';

(async function () {
    try {
        const server: Application = express();
        const PORT: any = process.env.PORT;

        server.use(express.json());
        server.use(express.urlencoded({ extended: true }));
        server.use(cors());

        server.use('/api/v1/family', familyRoutes);

        const connection: any = await db();

        const query = await connection.query('SELECT NOW()');

        console.log(Object.values(query[0])[0]);

        server.listen(PORT, () => console.log(`server is listening on port ${PORT}`));
    } catch (error) {
        console.log(error);
    }
})();

// process.on('uncaughtException', () => {
//     process.on('beforeExit', () => {
//         console.log('unhandled exception detected, process will exit');
//     });
//     process.exit(1);
// });
 