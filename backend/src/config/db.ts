import { createConnection } from 'typeorm';
import 'reflect-metadata';

export default async function () {
    try {
        return await createConnection();
    } catch (error) {
        console.log(error);
    }
}
