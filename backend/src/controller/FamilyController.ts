import { Request, Response } from 'express';
import reponseBody from '../model/response';
import familyService from '../service/FamilyService';

export default {
    getFamilyMemberTree: async (request: Request, response: Response) => {
        const service = await familyService.getFamilyMemberTree(request);

        return response.status(service.code).json(reponseBody(service));
    },

    getFamilyMemberList: async (request: Request, response: Response) => {
        const service = await familyService.getFamilyMemberList(request);

        return response.status(service.code).json(reponseBody(service));
    },
    addFamilyMember: async (request: Request, response: Response) => {
        const service = await familyService.addFamilyMember(request);
        return response.status(service.code).json(reponseBody(service));
    },
    editFamilyMember: async (request: Request, response: Response) => {
        const service = await familyService.editFamilyMember(request);

        return response.status(service.code).json(reponseBody(service));
    },
    removeFamilyMember: async (request: Request, response: Response) => {
        const service = await familyService.deleteFamilyMember(request);

        return response.status(service.code).json(reponseBody(service));
    },
};
