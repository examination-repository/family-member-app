import { Entity, PrimaryGeneratedColumn, TableForeignKey, Column } from 'typeorm';

@Entity()
export class Person {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 128 })
    person_code: string;

    @Column('varchar', { length: 50 })
    first_name: string;

    @Column('varchar', { length: 50 })
    middle_name: string;

    @Column('varchar', { length: 50 })
    last_name: string;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP()' })
    created_datetime: Date;

    @Column({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP()',
        onUpdate: 'CURRENT_TIMESTAMP()',
    })
    updated_datetime: Date;
}
