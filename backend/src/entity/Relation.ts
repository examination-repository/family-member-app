import { Entity, PrimaryGeneratedColumn, TableForeignKey, Column } from 'typeorm';

@Entity()
export class Relation {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 128 })
    person_code_1: string;

    @Column('varchar', { length: 128 })
    person_code_2: string;
}
