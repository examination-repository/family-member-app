import { Entity, PrimaryGeneratedColumn, TableForeignKey, Column } from 'typeorm';

@Entity()
export class RelationType {
    @Column('int')
    id: number;

    @Column('varchar', { length: 20 })
    description: string;
}
