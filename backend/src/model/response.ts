export default function (response: { code: number; message: string; data: any }) {
    return {
        code: response.code,
        message: response.message,
        data: response.data,
    };
}
