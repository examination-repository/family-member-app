import { Router } from 'express';

const router: Router = Router();

//* import all controller in here
import familyController from '../controller/FamilyController';

//* importal helper function in here
import { asyncHandler } from '../utils/AsyncTryCatchWrapper';

router.get('/', asyncHandler(familyController.getFamilyMemberList));
router.post('/:id', asyncHandler(familyController.addFamilyMember));
router.get('/:id', asyncHandler(familyController.getFamilyMemberTree));
router.put('/:id', asyncHandler(familyController.editFamilyMember));
router.delete('/:id', asyncHandler(familyController.removeFamilyMember));

export default router;
