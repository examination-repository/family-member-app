import db from '../config/db';
import { Person } from '../entity/Person';
import { Relation } from '../entity/Relation';
import { RelationType } from '../entity/RelationType';

export default {
    getFamilyMemberTree: async (request: any) => {
        return {
            code: 200,
            message: '',
            data: {},
        };
    },

    getFamilyMemberList: async (request: any) => {
        return {
            code: 200,
            message: '',
            data: [],
        };
    },
    addFamilyMember: async (request: any) => {
        return {
            code: 201,
            message: '',
            data: {},
        };
    },

    editFamilyMember: async (request: any) => {
        return {
            code: 201,
            message: '',
            data: {},
        };
    },

    deleteFamilyMember: async (request: any) => {
        return {
            code: 201,
            message: '',
            data: {},
        };
    },
};
